import argparse
import re


def genMem(filename):
	regex=re.compile("{.*(0x)?[0-9a-fA-F],.*(0x)?[0-9a-fA-F],.*(0x)?[0-9a-fA-F]}")
	decreg=re.compile("[0-9]")
	fw=open("si5338ROM.mem", 'w')
	fw.write("#OEB_ALL = 1\n#register: 230, data: 0x10, bitmask: 0xFF\n00:e610FF\n#DIS_LOL = 1\n#register: 241, data: 0xE5, bitmask: 0xFF\n01:f1E5FF\n")
	counter=2
	with open(filename) as f:
		for line in f:
			reg=regex.search(line)
			if reg:
				register=reg.group().replace("{","").replace("}","").replace(" ","")
				register=register.split(",")
				if register[2]!="0x00":
					fw.write("#register: "+register[0]+", data: "+register[1]+", bitmask: "+register[2]+"\n")
					if int(register[0])==255 and register[1]=='0x01':
	 					fw.write("# set page bit to 1\n")
	 				if int(register[0])==255 and register[1]=='0x00':
	 					fw.write("# set page bit to 0\n")
	 				temp=""
	 				if "0x" in register[1]:
	 					temp='{:02x}'.format(counter)+":"+'{:02x}'.format(int(register[0]))+register[1][2:]+register[2][2:]+"\n"
					else:
						temp='{:02x}'.format(counter)+":"+'{:02x}'.format(int(register[0]))+'{:02x}'.format(int(register[1]))+register[2][2:]+"\n"
					fw.write(temp.lower())
					counter+=1
	fw.close()

if __name__ == "__main__":
	parser=argparse.ArgumentParser()
	parser.add_argument("-filename","-f",required=True)
	args=parser.parse_args()
	genMem(args.filename)
	
